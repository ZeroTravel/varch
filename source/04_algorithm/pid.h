/*********************************************************************************************************
 *  ------------------------------------------------------------------------------------------------------
 *  file description
 *  ------------------------------------------------------------------------------------------------------
 *         \file  pid.h
 *         \unit  pid
 *        \brief  This is a general-purpose C language pid module
 *       \author  Lamdonn
 *      \version  v1.0.0
 *      \license  GPL-2.0
 *    \copyright  Copyright (C) 2023 Lamdonn.
 ********************************************************************************************************/
#ifndef __pid_H
#define __pid_H

#define PID_V_MAJOR                         1
#define PID_V_MINOR                         0
#define PID_V_PATCH                         0

/* PID controller structure */
typedef struct 
{
    /**< Set point or target value */
    double point;

    /**< Current value of process variable */
    double process;

    /**< Proportional coefficient */
    double kp;

    /**< Integration coefficient */
    double ki;

    /**< Differential coefficient */
    double kd;

    /**< Integral item */
    double integral;

    /**< Previous error */
    double error;

    /**< Controller output */
    double output;
} PIDC;

/**
* \brief Initializes the members of a PID controller structure.
*
* This function initializes the members of a PIDC structure with default values.
*
* \param[in] pid A pointer to the PIDC structure to be initialized.
*/
void pid_init(PIDC *pid);

/**
* \brief Computes the output of a PID controller.
*
* This function takes a PIDC structure pointer as input and computes the output
* of a PID controller based on the current error, previous error, integral term,
* and derivative term.
*
* \param[in] pid A pointer to the PIDC structure containing the PID controller parameters and variables.
*
* \return The output of the PID controller.
*/
double pid_compute(PIDC *pid);


#endif 
