#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "init.h"
#include "kern.h"
#include "oscp.h"
#include <sys/time.h>

int level = 0;

static unsigned int get_msec(void)
{
    struct timeval mstime;
    unsigned int ms = 0;
    gettimeofday(&mstime, NULL);
    ms = mstime.tv_sec * 1000 + mstime.tv_usec / 1000;
    return ms;
}

void task1(void)
{
    oscp_handle();
}

void task2(void)
{
    static int count = 0;
    
    count++;
    if (count > 628) count = 0;

    level = RESOLUTION / 2 * sin((double)count / 10) + RESOLUTION / 2;
}

static void test(void)
{
    if (kern_init(get_msec, 1) == KE_OK)
    {
        printf("kern init success!\r\n");
    }
    else  
    {
        printf("*** kern init fail!\r\n");
        return;
    }

    printf("create task %d\r\n", task_create(5, task1));
    printf("create task %d\r\n", task_create(50, task2));
    
    oscp_set_scale(O_SCALE_50MS);
    oscp_set_monitor(&level);

    kern_schedule();
}
init_export_app(test);
