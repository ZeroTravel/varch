#include <stdio.h>
#include <string.h>
#include "init.h"
#include "valloc.h"
#include "vector.h"

#define up_multiple(x, mul) ((x)+((mul)-((x)-1)%(mul))-1) /* get the smallest 'mul' multiple larger than 'x' */
static int gradient_capacity(int size)
{
    int capacity = 1;
    if (size <= 1) return 1;
    while (capacity < size) capacity <<= 1;
    capacity >>= 1;
    if (capacity < 4) capacity = capacity << 1;
    else if (capacity < 16) capacity = up_multiple(size, capacity >> 1);
    else if (capacity < 256) capacity = up_multiple(size, capacity >> 2);
    else capacity = up_multiple(size, 64);
    return capacity;
}

static void test(void)
{
    int size = 0, capacity = 0;
    for (size = 1; size < 1024; )
    {
        capacity = gradient_capacity(size);
        printf("+%d\t%d\r\n", capacity - size + 1, capacity);
        size = capacity + 1;
    }
    
    v_check_unfree();
}
init_export_app(test);
