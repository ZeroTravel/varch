#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "tree.h"

static void print_int(tree_t tree)
{
    if (tree_data(tree)) printf("%d", *(int *)tree_data(tree));
}

static void print_dir(tree_t tree)
{
    if (tree_data(tree)) printf("%s", (char *)tree_data(tree));
}

static void print_ini(tree_t tree)
{
    if (!tree_asize(tree)) return;
    printf("%s", (char *)tree_attribute(tree));
    if (tree_dsize(tree)) printf(" = %s", (char *)tree_data(tree));
}

static void print_bitree(tree_t tree)
{
    if (tree_dsize(tree)) printf("%s", (char *)tree_data(tree));
}

#if 0
#include "ini.h"
static tree_t tree_from_ini(const char *filename)
{
    tree_t tree = NULL, section, pair;
    ini_t ini;
    char *section_name, *key, *value;
    int i, j;
    ini = ini_file_load(filename);
    if (!ini) goto FAIL;
    tree = tree_create();
    if (!tree) goto FAIL;
    for (i = 0; i < ini_section_count(ini); i++)
    {
        section = tree_create();
        if (!section) goto FAIL;
        tree_insert(tree, tree_csize(tree));
        tree_attach(tree, tree_csize(tree) - 1, section);
        section_name = (char*)ini_section_name(ini, i);
        tree_set_attribute(section, section_name, strlen(section_name) + 1);
        for (j = 0; j < ini_pair_count(ini, section_name); j++)
        {
            pair = tree_create();
            if (!pair) goto FAIL;
            tree_insert(section, tree_csize(section));
            tree_attach(section, tree_csize(section) - 1, pair);
            key = (char *)ini_key_name(ini, section_name, j);
            tree_set_attribute(pair, key, strlen(key) + 1);
            value = (char *)ini_get_value(ini, section_name, key);
            tree_set_data(pair, value, strlen(value) + 1);
        }
    }
    ini_delete(ini);
    return tree;
FAIL:
    if (ini) ini_delete(ini);
    if (tree) tree_delete(tree, NULL);
    return NULL;
}

static void test_ini(void)
{
    tree_t tree, n;
    tree = tree_from_ini("../source/application/test/file/read.ini");
    if (!tree) 
    {
        printf("transfer fail!\r\n");
    }
    tree_print(tree_child(tree, 0), 0, print_ini);
    tree_delete(tree, NULL);
}
#endif 

#include <dirent.h>
#include <sys/stat.h>
static tree_t tree_directory(const char *directory, int depth)
{
    tree_t tree = NULL, n;
    DIR *dirptr;
    struct dirent* dircontent;
    if (depth < 0) return NULL;
    tree = tree_create();
    if (!tree) return NULL;
    dirptr = opendir(directory);
    if (!dirptr) goto FAIL;
    while (1)
    {
        dircontent = readdir(dirptr);
        if (dircontent == NULL) break;
        if (strcmp(dircontent->d_name,".") == 0 || strcmp(dircontent->d_name,"..") == 0) continue;
        struct stat statinfo;
        char fullname[1024];
        sprintf(fullname, "%s/%s", directory, dircontent->d_name);
        if (stat(fullname, &statinfo)) continue;
        if (S_ISDIR(statinfo.st_mode) && depth != 1) n = tree_directory(fullname, depth ? depth - 1 : 0);
        else if (S_ISREG(statinfo.st_mode)) n = tree_create();
        else continue;
        if (n)
        {
            if (!tree_insert(tree, tree_csize(tree))) goto FAIL;
            tree_attach(tree, tree_csize(tree) - 1, n);
        }
        tree_set_data(n, dircontent->d_name, strlen(dircontent->d_name) + 1);
    }
    if (!tree_data(tree)) tree_set_data(tree, directory, strlen(directory) + 1);
    return tree;
FAIL:
    if (tree) tree_delete(tree, NULL);
    return NULL;
}

static void test_dir(void)
{
    tree_t tree;
    tree = tree_directory("./", 0);
    if (!tree) 
    {
        printf("load fail!\r\n");
    }
    // tree_insert(tree, 1);
    // printf("name %s, %d\r\n", tree_data(tree), tree_size(tree));
    tree_print(tree, 0, print_dir);
    tree_delete(tree, NULL);
}

static void test_create(void)
{
    tree_t root, n, t;
    int i, data;
    
    root = tree_create();
    data = 1024; tree_set_data(root, &data, sizeof(data));

    for (i = 0; i < 12; i++)
    {
        n = tree_create();
        if (!n) break;
        tree_insert(root, tree_csize(root));
        tree_attach(root, tree_csize(root) - 1, n);
        data = i; tree_set_data(n, &data, sizeof(data));
    }

    t = tree_to(root, 3);
    for (i = 0; i < 8; i++)
    {
        n = tree_create();
        if (!n) break;
        tree_insert(t, tree_csize(t));
        tree_attach(t, tree_csize(t) - 1, n);
        data = i + 30; tree_set_data(n, &data, sizeof(data));
    }

    t = tree_to(root, 3, 6);
    for (i = 0; i < 4; i++)
    {
        n = tree_create();
        if (!n) break;
        tree_insert(t, tree_csize(t));
        tree_attach(t, tree_csize(t) - 1, n);
        data = i + 360; tree_set_data(n, &data, sizeof(data));
    }

    printf("tree size %d\r\n", tree_size(root));
    printf("tree depth %d\r\n", tree_depth(root));
    /* show */
    tree_print(root, 0, print_int);
    tree_delete(root, NULL);
}

static tree_t bitree_gen(int depth)
{
    tree_t tree;
    if (depth <= 0) return NULL;
    tree = tree_create();
    tree_insert(tree, 0);
    tree_insert(tree, 0);
    tree_attach(tree, 0, bitree_gen(depth - 1));
    tree_attach(tree, 1, bitree_gen(depth - 1));
    return tree;
}



static void test_bitree(void)
{
    tree_t tree = bitree_gen(5);
    tree_set_data(tree_to(tree, 0, 0, 0), "123", strlen("123") + 1);
    tree_print(tree, 0, print_bitree);
    tree_delete(tree, NULL);
}

static void test(void)
{
    printf("tree test\r\n");

    // test_create();
    test_dir();
    // test_ini();
    // test_bitree();

    v_check_unfree();
    // printf("used %d\r\n", v_check_used());
}
init_export_app(test);
