#include <stdio.h>
#include <string.h>
#include "init.h"
#include "txls.h"
#include "valloc.h"

#define READ_FILE "test/file/read.md"
#define WRITE_FILE "test/file/write.md"

void txls_preview(txls_t txls)
{
    char* s;
    int len = 0;
    if (!txls) return;
    s = txls_dumps(txls, 1, &len);
    printf("s %p\r\n", s);
    if (!s) return;
    printf("len = %d, <%d, %d>\r\n%s\r\n", len, txls_col(txls), txls_row(txls), s);
    free(s);
}

static void test_read(void)
{
    txls_t x = NULL;  // 定义txls对象，习惯初始化为NULL

    /* 加载txls文件 */
    x = txls_file_load(READ_FILE);
    if (!x) // 加载失败，定位错误
    {
        int line, type;
        type = txls_error_info(&line);
        printf("txls parse error! line %d, error %d.\r\n", line, type);
        return;
    }

    /* 遍历表头，定位所在列 */
    int col = 0;
    for (col = 1; col <= txls_col(x); col++)
    {
        if (strcmp("Li Si", txls_get_head(x, col)) == 0)
        {
            break;
        }
    }
    if (col > txls_col(x)) // 没有查找到
    {
        printf("Lookup failed\r\n");
        return;
    }

    /* 打印信息 */
    printf("name: %s, age=%s, gender: %s, height=%s, weight=%s, email:%s\r\n", 
    txls_get_text(x, col, 0),
    txls_get_text(x, col, 1),
    txls_get_text(x, col, 2),
    txls_get_text(x, col, 3),
    txls_get_text(x, col, 4),
    txls_get_text(x, col, 5));

    txls_delete(x);
}

static void test_write(void)
{
    txls_t x = NULL;  // 定义txls对象，习惯初始化为NULL

    x = txls_create(4, 5); // 创建4x5的表格
    if (!x)
    {
        return;
    }

    /* 设置表头，第一列留空 */
    txls_set_head(x, 2, "Zhang San");
    txls_set_head(x, 3, "Li Si");
    txls_set_head(x, 4, "Wang Wu");

    /* 设置对齐方式 */
    txls_set_align(x, 2, TXLS_ALIGN_LEFT);
    txls_set_align(x, 3, TXLS_ALIGN_CENTER);
    txls_set_align(x, 4, TXLS_ALIGN_RIGHT);

    /* 第一列作为信息类别 */
    txls_set_text(x, 1, 1, "age");
    txls_set_text(x, 1, 2, "gender");
    txls_set_text(x, 1, 3, "height");
    txls_set_text(x, 1, 4, "weight");
    txls_set_text(x, 1, 5, "email");

    /* 写入每个人信息 */
    // Zhang San
    txls_set_text(x, 2, 1, "18");
    txls_set_text(x, 2, 2, "man");
    txls_set_text(x, 2, 3, "178.5");
    txls_set_text(x, 2, 4, "65");
    txls_set_text(x, 2, 5, "123321@qq.com");
    // Li Si
    txls_set_text(x, 3, 1, "24");
    txls_set_text(x, 3, 2, "woman");
    txls_set_text(x, 3, 3, "165");
    txls_set_text(x, 3, 4, "48");
    txls_set_text(x, 3, 5, "lisi@163.com");
    // Wang Wu
    txls_set_text(x, 4, 1, "20");
    txls_set_text(x, 4, 2, "man");
    txls_set_text(x, 4, 3, "175");
    txls_set_text(x, 4, 4, "75");
    txls_set_text(x, 4, 5, "ww1234567890@qq.com");

    txls_file_dump(x, WRITE_FILE);

    txls_delete(x);
}

static void test(void)
{
    // test_write();
    test_read();

    // v_check_unfree();
}
init_export_app(test);
