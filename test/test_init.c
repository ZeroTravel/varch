#include <stdio.h>
#include "init.h"

void test_init_hardware(void)
{
    printf("hardware init!\r\n");
}
init_export_hardware(test_init_hardware);

void test_init_driver(void)
{
    printf("driver init!\r\n");
}
init_export_driver(test_init_driver);

void test_init_system(void)
{
    printf("system init!\r\n");
}
init_export_system(test_init_system);

void test_init_module(void)
{
    printf("module init!\r\n");
}
init_export_module(test_init_module);

void test_init_app(void)
{
    printf("app init!\r\n");
}
init_export_app(test_init_app);

void test_init_system1(void)
{
    printf("system1 init!\r\n");
}
init_export_system(test_init_system1);
