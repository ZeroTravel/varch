#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "stack.h"

static void test_stack(void) 
{
    stack_t stack = stack(int, 10);
    int i = 0;

    for (i = 0; i < stack_capacity(stack); i++)
    {
        stack_push(stack, &i);
    }
    stack_pop(stack, NULL);
    stack_pop(stack, NULL);
    for (i = 0; i < stack_size(stack); i++)
    {
        printf("stack[%d] = %d\r\n", i, stack_at(stack, int, i));
    }

    _stack(stack);
}

static void test(void) 
{
    test_stack();
    v_check_unfree();
}
init_export_app(test);
