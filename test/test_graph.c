#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "graph.h"
#include "arg.h"

#define la(type, value)                 ((type[1]){value})

void graph_traverse_int(int index, void *data, int size)
{
    printf("graph[%d] %d\r\n", index, *(int *)data);
}

static void test_create(void)
{
    graph_t graph = NULL;
    int i = 25;
    int index = -1;
    
    graph = graph_create(100, 1);
    if (!graph)
    {
        printf("graph_create fail!\r\n");
    }


    index = graph_add_vertex(graph, la(int, 12), sizeof(int));
    printf("index %d\r\n", index);

    index = graph_add_vertex(graph, la(int, 13), sizeof(int));
    printf("index %d\r\n", index);

    index = graph_add_vertex(graph, la(int, 15), sizeof(int));
    printf("index %d\r\n", index);

    index = graph_add_vertex(graph, la(int, 23), sizeof(int));
    printf("index %d\r\n", index);

    printf("graph_add_edge %d\r\n", graph_add_edge(graph, 0, 1, 0));
    printf("graph_add_edge %d\r\n", graph_add_edge(graph, 0, 2, 0));
    printf("graph_add_edge %d\r\n", graph_add_edge(graph, 2, 3, 0));

    // graph_remove_edge(graph, 0, 2);
    // graph_remove_vertex(graph, 0);

    printf("ret %d\r\n", graph_vertex_set_data(graph, 1, la(int, 1024), sizeof(int)));

    printf("graph_vertex_data %d\r\n", *(int *)graph_vertex_data(graph, 1, NULL));

    graph_dfs(graph, 0, graph_traverse_int);
    printf("----------------------\r\n");
    graph_bfs(graph, 0, graph_traverse_int);

    graph_delete(graph);
}

static void test_add_vertex(void)
{
    graph_t graph = NULL;
    
    graph = graph_create(10, 1);
    if (!graph)
    {
        printf("graph_create fail!\r\n");
    }

    graph_add_vertex(graph, la(int, 12), sizeof(int));
    graph_add_vertex(graph, la(int, 13), sizeof(int));
    graph_add_vertex(graph, la(int, 15), sizeof(int));
    graph_add_vertex(graph, la(int, 23), sizeof(int));

    graph_ls(graph, graph_traverse_int);

    graph_delete(graph);
}

static void test_add_edge(void)
{
    graph_t graph = NULL;
    
    graph = graph_create(10, 1);
    if (!graph)
    {
        printf("graph_create fail!\r\n");
    }

    graph_add_vertex(graph, la(int, 12), sizeof(int));
    graph_add_vertex(graph, la(int, 13), sizeof(int));
    graph_add_vertex(graph, la(int, 15), sizeof(int));
    graph_add_vertex(graph, la(int, 23), sizeof(int));

    printf("---------------\r\n");

    graph_dfs(graph, 0, graph_traverse_int); 

    printf("---------------\r\n");

    graph_add_edge(graph, 0, 1, 0);
    graph_add_edge(graph, 0, 2, 0);
    graph_add_edge(graph, 0, 3, 0);
    graph_add_edge(graph, 1, 0, 0);
    graph_add_edge(graph, 1, 2, 0);
    graph_add_edge(graph, 1, 3, 0);
    graph_add_edge(graph, 2, 3, 0);

    graph_dfs(graph, 0, graph_traverse_int); 

    printf("---------------\r\n");

    graph_delete(graph);
}

static void test_degree(void)
{
    graph_t graph = NULL;
    
    graph = graph_create(10, 1);
    if (!graph)
    {
        printf("graph_create fail!\r\n");
    }

    graph_add_vertex(graph, la(int, 100), sizeof(int));
    graph_add_vertex(graph, la(int, 200), sizeof(int));
    graph_add_vertex(graph, la(int, 300), sizeof(int));
    graph_add_vertex(graph, la(int, 500), sizeof(int));

    graph_add_edge(graph, 0, 1, 0);
    graph_add_edge(graph, 0, 2, 0);
    graph_add_edge(graph, 0, 3, 0);
    graph_add_edge(graph, 1, 0, 0);
    graph_add_edge(graph, 1, 2, 0);
    graph_add_edge(graph, 1, 3, 0);
    graph_add_edge(graph, 2, 3, 0);

    printf("graph_out_degree %d\r\n", graph_out_degree(graph, 3));
    printf("graph_out_degree %d\r\n", graph_out_degree(graph, 2));

    printf("graph_in_degree %d\r\n", graph_in_degree(graph, 0));
    printf("graph_in_degree %d\r\n", graph_in_degree(graph, 2));

    graph_delete(graph);
}

static void test_remove(void)
{
    graph_t graph = NULL;
    
    graph = graph_create(100, 1);
    if (!graph)
    {
        printf("graph_create fail!\r\n");
    }

    graph_add_vertex(graph, la(int, 100), sizeof(int));
    graph_add_vertex(graph, la(int, 200), sizeof(int));
    graph_add_vertex(graph, la(int, 300), sizeof(int));
    graph_add_vertex(graph, la(int, 500), sizeof(int));

    graph_add_edge(graph, 0, 1, 0);
    graph_add_edge(graph, 0, 2, 0);
    graph_add_edge(graph, 0, 3, 0);
    graph_add_edge(graph, 1, 0, 0);
    graph_add_edge(graph, 1, 2, 0);
    graph_add_edge(graph, 1, 3, 0);
    graph_add_edge(graph, 2, 3, 0);

    graph_remove_vertex(graph, 1);
    graph_remove_edge(graph, 0, 2);

    graph_dfs(graph, 0, graph_traverse_int);

    graph_delete(graph);
}

static void test_search(void)
{
    graph_t graph = NULL;
    
    graph = graph_create(100, 1);
    if (!graph)
    {
        printf("graph_create fail!\r\n");
    }

    graph_add_vertex(graph, la(int, 100), sizeof(int));
    graph_add_vertex(graph, la(int, 200), sizeof(int));
    graph_add_vertex(graph, la(int, 300), sizeof(int));
    graph_add_vertex(graph, la(int, 500), sizeof(int));

    graph_add_edge(graph, 0, 1, 0);
    graph_add_edge(graph, 0, 2, 0);
    graph_add_edge(graph, 0, 3, 0);
    graph_add_edge(graph, 1, 0, 0);
    graph_add_edge(graph, 1, 2, 0);
    graph_add_edge(graph, 1, 3, 0);
    graph_add_edge(graph, 2, 3, 0);

    graph_remove_vertex(graph, 1);
    graph_remove_edge(graph, 0, 2);

    printf("graph_ls ----------------------\r\n");
    graph_ls(graph, graph_traverse_int);
    printf("graph_dfs ----------------------\r\n");
    graph_dfs(graph, 0, graph_traverse_int);
    printf("graph_bfs ----------------------\r\n");
    graph_bfs(graph, 0, graph_traverse_int);

    graph_delete(graph);
}

static void test_data(void)
{
    graph_t graph = NULL;
    
    graph = graph_create(100, 1);
    if (!graph)
    {
        printf("graph_create fail!\r\n");
    }

    graph_add_vertex(graph, la(int, 100), sizeof(int));
    graph_add_vertex(graph, la(int, 200), sizeof(int));
    graph_add_vertex(graph, la(int, 300), sizeof(int));
    graph_add_vertex(graph, la(int, 500), sizeof(int));

    graph_vertex_set_data(graph, 2, la(int, 1024), sizeof(int));

    int size = 0; 
    printf("graph_vertex_data[3].data = %d\r\n", *(int *)graph_vertex_data(graph, 3, &size));
    printf("graph_vertex_data[3].size = %d\r\n", size);

    graph_ls(graph, graph_traverse_int);

    graph_delete(graph);
}

static void test_weight(void)
{
    graph_t graph = NULL;
    
    graph = graph_create(100, 1);
    if (!graph)
    {
        printf("graph_create fail!\r\n");
    }

    graph_add_vertex(graph, la(int, 100), sizeof(int));
    graph_add_vertex(graph, la(int, 200), sizeof(int));
    graph_add_vertex(graph, la(int, 300), sizeof(int));
    graph_add_vertex(graph, la(int, 500), sizeof(int));

    graph_add_edge(graph, 0, 1, 0);
    graph_add_edge(graph, 0, 2, 0);
    graph_add_edge(graph, 0, 3, 10);
    graph_add_edge(graph, 1, 0, 0);
    graph_add_edge(graph, 1, 2, 0);
    graph_add_edge(graph, 1, 3, 0);
    graph_add_edge(graph, 2, 3, 0);

    printf("graph_get_edge_weight = %d\r\n", graph_get_edge_weight(graph, 0, 3));
    graph_set_edge_weight(graph, 0, 3, 1024);
    printf("graph_get_edge_weight = %d\r\n", graph_get_edge_weight(graph, 0, 3));

    graph_delete(graph);
}

static void test_shortest_path(void)
{
    graph_t graph = NULL;
    
    graph = graph_create(100, 1);
    if (!graph)
    {
        printf("graph_create fail!\r\n");
    }

    graph_add_vertex(graph, la(int, 100), sizeof(int));
    graph_add_vertex(graph, la(int, 200), sizeof(int));
    graph_add_vertex(graph, la(int, 300), sizeof(int));
    graph_add_vertex(graph, la(int, 500), sizeof(int));

    graph_add_edge(graph, 0, 1, 3);
    graph_add_edge(graph, 0, 2, 2);
    graph_add_edge(graph, 0, 3, 5);
    graph_add_edge(graph, 1, 0, 6);
    graph_add_edge(graph, 1, 2, 3);
    graph_add_edge(graph, 1, 3, 2);
    graph_add_edge(graph, 2, 3, 1);

    graph_shortest_path(graph, 0);

    graph_delete(graph);
}

static void test_min_cover(void)
{
    graph_t graph = NULL;
    
    graph = graph_create(100, 1);
    if (!graph)
    {
        printf("graph_create fail!\r\n");
    }

    graph_add_vertex(graph, la(int, 100), sizeof(int));
    graph_add_vertex(graph, la(int, 200), sizeof(int));
    graph_add_vertex(graph, la(int, 300), sizeof(int));
    graph_add_vertex(graph, la(int, 500), sizeof(int));

    graph_add_edge(graph, 0, 1, 3);
    graph_add_edge(graph, 0, 2, 2);
    graph_add_edge(graph, 0, 3, 5);
    graph_add_edge(graph, 1, 0, 6);
    graph_add_edge(graph, 1, 2, 3);
    graph_add_edge(graph, 1, 3, 2);
    graph_add_edge(graph, 2, 3, 1);

    graph_min_vertex_cover(graph);

    graph_delete(graph);
}

static void test_others(void)
{
    graph_t graph = NULL;
    
    graph = graph_create(100, 1);
    if (!graph)
    {
        printf("graph_create fail!\r\n");
    }

    graph_add_vertex(graph, la(int, 100), sizeof(int));
    graph_add_vertex(graph, la(int, 200), sizeof(int));
    graph_add_vertex(graph, la(int, 300), sizeof(int));
    graph_add_vertex(graph, la(int, 500), sizeof(int));

    graph_add_edge(graph, 0, 1, 3);
    graph_add_edge(graph, 0, 2, 2);
    graph_add_edge(graph, 0, 3, 5);
    graph_add_edge(graph, 1, 0, 6);
    graph_add_edge(graph, 1, 2, 3);
    graph_add_edge(graph, 1, 3, 2);
    graph_add_edge(graph, 2, 3, 1);

    // graph_topological_sort(graph);
    // graph_shortest_path(graph, 0);
    // graph_minimum_spanning_tree(graph);
    // printf("graph_is_connected %d\r\n", graph_is_connected(graph));
    // printf("graph_is_complete %d\r\n", graph_is_complete(graph));
    // printf("graph_is_bipartite %d\r\n", graph_is_bipartite(graph));
    // printf("graph_is_eulerian %d\r\n", graph_is_eulerian(graph));
    // printf("graph_is_eulerian %d\r\n", graph_max_flow(graph, 0, 3));
    // graph_articulation_points(graph);
    // graph_bridges(graph);
    graph_min_vertex_cover(graph);

    graph_dfs(graph, 0, graph_traverse_int);

    graph_delete(graph);
}

static void test(void)
{
    printf("graph test!\r\n");
    
    // test_create();
    // test_add_vertex();
    // test_add_edge();
    // test_remove();
    // test_search();
    // test_data();
    // test_degree();
    // test_weight();
    // test_shortest_path();
    test_min_cover();
    
    v_check_unfree();
}
init_export_app(test);
