#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "pid.h"

static void test_sim(void)
{
    PIDC pid;

    pid_init(&pid);

    pid.kp = 2;
    pid.ki = 0.1;
    pid.kd = 0.05;

    pid.point = 100.0;

    /* Simulate the process, assuming that the value of each cycle process increases by 5 */
    for (int i = 0; i < 20; i++) 
    {
        pid.process += 5;
        pid_compute(&pid);

        printf("Process Value: %.2f, PID Output: %.2f\n", pid.process, pid.output);
    }
}

static void test(void)
{
    printf("pid test!\r\n");
    
    test_sim();
}
init_export_app(test);
