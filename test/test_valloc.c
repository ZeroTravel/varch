#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "valloc.h"

static void test(void)
{
    void* p = NULL;
    printf("malloc %p\r\n", malloc(0));
    p = realloc(NULL, 100);
    p = malloc(64);
    p = malloc(50);
    printf("realloc %p\r\n", realloc(NULL, 0));
    printf("%d\r\n", *(int *)p);
    free(p);

    v_check_unfree();
    printf("count = %d\r\n", v_check_count());
    printf("used = %d\r\n", v_check_used());
}
init_export_app(test);