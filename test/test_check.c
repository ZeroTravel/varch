#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "check.h"

typedef uint8_t (*checkType)(uint8_t* data, uint32_t len);

static checkType checkFunTable[] = {
    check_sum,
    check_parity,
    check_lrc,
    check_xor,
};

static void test(void)
{
    char *testSample[] = {
        "Hello",
        "check algorithms",
        "123456789",
    };
    uint8_t check = 0;
    for (int i = 0; i < sizeof(testSample) / sizeof(testSample[0]); i++)
    {
        printf("testSample %d\r\n", i);
        for (int j = 0; j < sizeof(checkFunTable) / sizeof(checkFunTable[0]); j++)
        {
            check = (checkFunTable[j])(testSample[i], strlen(testSample[i]));
            printf("Func %d, check %02X\r\n", j, check);
        }
    }
}
init_export_app(test);
