#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "map.h"

static void test_it(void)
{
    map_t map = map(string, int);
    int value;
    char *key;
    void *data;
    int i;

    value = 100; map_insert(map, mpair("hello", &value));
    value = 1; map_insert(map, mpair("ZhangSan", &value));
    value = 2; map_insert(map, mpair("LiSi", &value));
    value = 3; map_insert(map, mpair("WangWu", &value));
    value = 4; map_insert(map, mpair("SunLiu", &value));
    value = 5; map_insert(map, mpair("QianQi", &value));

    map_it_init(map, MAP_HEAD);
    i = map_size(map);
    while (i--)
    {
        data = map_it_get(map, &key, NULL);
        printf("map[%s] = %d\r\n", key, *(int *)data);
    }

    _map(map);
}

static void test_map(void)
{
    int value;
    map_t map = map(string, int);
    if (!map) return;

    value = 100; map_insert(map, mpair("hello", &value));
    value = 110; map_insert(map, mpair("Zhang", &value));

    printf("size = %d, key size = %d, value size = %d\r\n", map_size(map), map_ksize(map), map_vsize(map));

    printf("map[hello] = %d\r\n", map_at(map, int, "hello"));
    printf("map[Zhang] = %d\r\n", map_at(map, int, "Zhang"));
    map_erase(map, "hello");

    _map(map);
}

static void test_float_map(void)
{
    int value;
    map_t map = map(double, int);
    if (!map) return;

    value = 111; map_insert(map, mpair(12.3, &value));
    value = 100; map_insert(map, mpair(16.9, &value));
    printf("%d\r\n", map_at(map, int, 16.9000000000000000000001));
    map_erase(map, 16.9);
    _map(map);
}

static void test(void) 
{
    test_it();
    // test_map();
    // test_float_map();
    v_check_unfree();
}
init_export_app(test);