#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "deque.h"

static void test_deque(void) 
{
    deque_t deque = deque(int, 10);
    int i = 0;

    for (i = 0; i < deque_capacity(deque); i++)
    {
        deque_push_back(deque, &i);
    }
    deque_pop_front(deque, NULL);
    deque_pop_back(deque, NULL);
    for (i = 0; i < deque_size(deque); i++)
    {
        printf("deque[%d] = %d\r\n", i, deque_at(deque, int, i));
    }

    _deque(deque);
}

static void test(void) 
{
    test_deque();
    v_check_unfree();
}
init_export_app(test);
