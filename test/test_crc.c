#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "crc.h"

uint32_t std_crc(uint8_t* data, uint32_t len, uint32_t index)
{
    uint32_t crc = 0;

    switch (index)
    {
    case 0: crc = crc4_itu(data, len); break;
    case 1: crc = crc5_epc(data, len); break;
    case 2: crc = crc5_itu(data, len); break;
    case 3: crc = crc5_usb(data, len); break;
    case 4: crc = crc6_itu(data, len); break;
    case 5: crc = crc7_mmc(data, len); break;
    case 6: crc = crc8(data, len); break;
    case 7: crc = crc8_itu(data, len); break;
    case 8: crc = crc8_rohc(data, len); break;
    case 9: crc = crc8_maxim(data, len); break;
    case 10: crc = crc16_ibm(data, len); break;
    case 11: crc = crc16_maxim(data, len); break;
    case 12: crc = crc16_usb(data, len); break;
    case 13: crc = crc16_modbus(data, len); break;
    case 14: crc = crc16_ccitt(data, len); break;
    case 15: crc = crc16_ccitt_false(data, len); break;
    case 16: crc = crc16_x25(data, len); break;
    case 17: crc = crc16_xmodem(data, len); break;
    case 18: crc = crc16_dnp(data, len); break;
    case 19: crc = crc32(data, len); break;
    case 20: crc = crc32_mpeg_2(data, len); break;
    default:
        break;
    }

    return crc;
}

static void test(void)
{
    char *testSample[] = {
        "Hello",
        "crc algorithms",
        "123456789",
    };
    uint32_t crc0 = 0, crc1 = 0;

    for (int i = 0; i < sizeof(testSample) / sizeof(testSample[0]); i++)
    {
        printf("testSample %d\r\n", i);
        for (int j = 0; j < sizeof(crcParaModelTable) / sizeof(crcParaModelTable[0]); j++)
        {
            crc0 = std_crc(testSample[i], strlen(testSample[i]), j);
            crc1 = crc(testSample[i], strlen(testSample[i]), &crcParaModelTable[j]);
            printf("crc %02d, crc0 %08X, crc1 %08X, same %d\r\n", j, crc0, crc1, (crc0 == crc1) ? 1 : 0);
        }
    }
}
init_export_app(test);
