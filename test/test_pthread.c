#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include <pthread.h>
#include <semaphore.h>

sem_t s;

static void *thread_entry(void *param)
{
    long thread_id = (long)param;
 
    for (;;)
    {
        sem_wait(&s);
        printf("in\r\n");
    }
    return NULL;
};


static void test(void)
{
    pthread_t t;
    pthread_create(&t, NULL, thread_entry, NULL);
    printf("pthread\r\n");
    sem_post(&s);
}
init_export_app(test);
