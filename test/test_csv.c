#include <stdio.h>
#include <string.h>
#include "init.h"
#include "csv.h"
#include "valloc.h"

#define READ_FILE "source/application/test/file/read.csv"
#define WRITE_FILE "source/application/test/file/write.csv"

static void test_dump(void)
{
    csv_t csv;
    const char *array[3][5] = {
        {"ID",          "Name",     "Gender",   "Age",  "Height"},
        {"20240107001", "ZhangSan", "Man",      "18",   "178"},
        {"20240107002", "LiSi",     "Woman",    "24",   "162"},
    };
    char *out[2][3];

    csv = csv_create(3, 5, array);
    if (!csv) 
    {
        printf("create csv fail!\r\n");
        return;
    }

    int row = csv_row(csv);
    int col = csv_col(csv);
    printf("row %d, col %d\r\n", row, col);

    csv_to_array(csv, 2, 4, out, 2, 3);

    printf("out: %s\r\n", out[1][2]);

#if 0
    csv_set_text(csv, 1, 1, "DID");
    csv_set_text(csv, 1, 2, "Function");
    csv_set_text(csv, 1, 3, "Comment");

    csv_set_text(csv, 2, 1, "$D131");
    csv_set_text(csv, 2, 2, "Sample ID");
    csv_set_text(csv, 2, 3, "Yes/No");

    csv_set_text(csv, 3, 3, "中国");

    csv_copy_cell_to(csv, 3,3,  3, 1);

    csv_cut_cell_to(csv, 3, 1, 7, 10);
#endif 
    // csv_insert_col(csv, 2);
    char *rows[5] = {"20240107005", "Wangwu",     "Woman",    "21",   "160"};
    csv_insert_row(csv, 0, rows, 5);
    char *cols[3] = {"Weight", "56", "62"};
    csv_insert_col(csv, 0, cols, 3);

    // csv_move_row_to(csv, 6, 1);
    // csv_move_col_to(csv, 6, 3);
    // csv_copy_row_to(csv, 4, 2);
    // csv_copy_col_to(csv, 4, 2);
    // csv_insert_cell(csv, 2, 4, 1);
    // csv_delete_cell(csv, 2, 4, 1);

    // csv_insert_col(csv, 0, NULL, 0);
    // csv_insert_col(csv, 0, NULL, 0);

    csv_minify(csv);

#if 0
    const char *text = NULL;
    csv_for_each(csv, row, col, text)
    {
        printf("[%d, %d]: %s\r\n", row, col, text);
    }
#endif 

    // printf("cell count %d.\r\n", csv_cell(csv));

    while (csv_find(csv, "6", CSV_F_FLAG_MatchForward | CSV_F_FLAG_MatchByCol, &row, &col) == 1)
    {
        printf("[%d, %d]: %s\r\n", row, col, csv_get_text(csv, row, col));
    }

    printf("File length %d.\r\n", csv_file_dump(csv, WRITE_FILE));

    csv_delete(csv);
}

static void test_load(void)
{
    csv_t csv;
    unsigned int row, col;
    int i, j;
    const char *text;
    int error;
    int line;
    int ecol;

    csv = csv_file_load(READ_FILE);
    if (!csv)
    {
        error = csv_error_info(&line, &ecol);
        printf("Load csv file fail! Error line %d column %d code %d.\r\n", line, ecol, error);
        return;
    }

    row = csv_row(csv);
    col = csv_col(csv);
    text = csv_get_text(csv, 1, 3);
    
    printf("len %d, %s\r\n", strlen(text), text);
    printf("row %d, col %d\r\n", row, col);

    for (i = 1; i <= row; i++)
    {
        for (j = 1; j <= col; j++)
        {
            text = csv_get_text(csv, i, j);
            if (text)
            {
                if (j != 1) printf(",");
                printf("%s", text);
            }
        }
        printf("\r\n");
    }

    csv_delete(csv);
}

static void dump_demo(void)
{
    csv_t csv;
    const char *array[3][5] = {
        {"ID",          "Name",     "Gender",   "Age",  "Height"},
        {"20240107001", "ZhangSan", "Man",      "18",   "178"},
        {"20240107002", "LiSi",     "Woman",    "24",   "162"},
    };

    csv = csv_create(3, 5, array);
    if (!csv) 
    {
        printf("create csv fail!\r\n");
        return;
    }

    if (csv_file_dump(csv, "info.csv") < 0)
    {
        printf("csv dump fail!\r\n");
    }
    else  
    {
        printf("csv dump success!\r\n");
    }

    csv_delete(csv);
}

static void load_demo(void)
{
    csv_t csv;

    csv = csv_file_load("info.csv");
    if (!csv) 
    {
        printf("csv load fail!\r\n");
        return;
    }

    unsigned int row, col;
    const char *text = NULL;
    csv_for_each(csv, row, col, text)
    {
        printf("[%u, %u]: %s\r\n", row, col, text);
    }

    csv_delete(csv);
}

static void test(void)
{
    // test_dump();
    // test_load();

    // dump_demo();
    load_demo();
    printf("use %d\r\n", v_check_used());
}
init_export_app(test);
