
TEST_SRC =
TEST_INC = 

#----------------------------------------
# test sources 
#----------------------------------------
# TEST_SRC += $(TESTSPACE)/test_init.c
# TEST_SRC += $(TESTSPACE)/test_kern.c
# TEST_SRC += $(TESTSPACE)/test_valloc.c
# TEST_SRC += $(TESTSPACE)/test_arg.c
# TEST_SRC += $(TESTSPACE)/test_vstd.c
# TEST_SRC += $(TESTSPACE)/test_vlog.c
# TEST_SRC += $(TESTSPACE)/test_ini.c
# TEST_SRC += $(TESTSPACE)/test_txls.c
# TEST_SRC += $(TESTSPACE)/test_xml.c
# TEST_SRC += $(TESTSPACE)/test_csv.c
# TEST_SRC += $(TESTSPACE)/test_json.c
# TEST_SRC += $(TESTSPACE)/test_vector.c
# TEST_SRC += $(TESTSPACE)/test_list.c
# TEST_SRC += $(TESTSPACE)/test_str.c
# TEST_SRC += $(TESTSPACE)/test_queue.c
# TEST_SRC += $(TESTSPACE)/test_stack.c
# TEST_SRC += $(TESTSPACE)/test_deque.c
# TEST_SRC += $(TESTSPACE)/test_set.c
# TEST_SRC += $(TESTSPACE)/test_dict.c
# TEST_SRC += $(TESTSPACE)/test_map.c
# TEST_SRC += $(TESTSPACE)/test_heap.c
# TEST_SRC += $(TESTSPACE)/test_tree.c
TEST_SRC += $(TESTSPACE)/test_graph.c
# TEST_SRC += $(TESTSPACE)/test_rbtree.c
# TEST_SRC += $(TESTSPACE)/test_pthread.c
# TEST_SRC += $(TESTSPACE)/test_encrypt.c
# TEST_SRC += $(TESTSPACE)/test_calculate.c
# TEST_SRC += $(TESTSPACE)/test_command.c
# TEST_SRC += $(TESTSPACE)/test_check.c
# TEST_SRC += $(TESTSPACE)/test_crc.c
# TEST_SRC += $(TESTSPACE)/test_sort.c
# TEST_SRC += $(TESTSPACE)/test_hash.c
# TEST_SRC += $(TESTSPACE)/test_pid.c
# TEST_SRC += $(TESTSPACE)/test_search.c
# TEST_SRC += $(TESTSPACE)/test_filter.c
# TEST_SRC += $(TESTSPACE)/test_oscp.c
# TEST_SRC += $(TESTSPACE)/test_tool.c
# TEST_SRC += $(TESTSPACE)/test_sList.c
# TEST_SRC += $(TESTSPACE)/test_dList.c
# TEST_SRC += $(TESTSPACE)/test_cQueue.c

export TEST_SRC TEST_INC
