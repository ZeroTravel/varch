#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "kern.h"
#include <sys/time.h>

static unsigned int get_msec(void)
{
    struct timeval mstime;
    unsigned int ms = 0;
    gettimeofday(&mstime, NULL);
    ms = mstime.tv_sec * 1000 + mstime.tv_usec / 1000;
    return ms;
}

void task1(void)
{
    static int count = 0;
    printf("task1 running! %d\r\n", ++count);
}

void task2(void)
{
    static int count = 0;
    printf("task2 running! %d\r\n", ++count);
}

static void test(void)
{
    if (kern_init(get_msec, 1) == KE_OK)
    {
        printf("kern init success!\r\n");
    }
    else  
    {
        printf("*** kern init fail!\r\n");
        return;
    }

    // printf("create task %d\r\n", task_create(1000, task1));
    // printf("create task %d\r\n", task_create(500, task2));

    // kern_schedule();
}
init_export_system(test);
