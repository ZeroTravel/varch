#include <stdio.h>
#include "init.h"
#include "ini.h"
#include "valloc.h"

#define READ_FILE "test/file/read.ini"
#define WRITE_FILE "test/file/write.ini"

static void test_dump(void)
{
    ini_t ini = NULL; // 定义ini对象，习惯初始化为NULL

    ini = ini_create(); // 创建空ini对象
    if (ini == NULL) 
    {
        printf("ini create fail!\r\n");
        return;
    } 

    /* 添加section */
    ini_add_section(ini, "Zhang San");
    ini_add_section(ini, "Li Si");
    ini_add_section(ini, "Wang Wu");

    /* 添加键值 */
    ini_set_value(ini, "Zhang San", "age", "18");
    ini_set_value(ini, "Zhang San", "height", "178");
    ini_set_value(ini, "Zhang San", "email", "123456@qq.com");

    ini_set_value(ini, "Li Si", "age", "20");
    ini_set_value(ini, "Li Si", "gender", "man");
    ini_set_value(ini, "Li Si", "weight", "65");

    ini_set_value(ini, "Wang Wu", "age", "22");

    /* 转储ini到文件 */
    ini_file_dump(ini, WRITE_FILE);

    ini_delete(ini); // 用完之后需要删除
}

static void test_load(void) 
{
    ini_t ini;
    char *p = NULL;
    int len = 0;

    ini = ini_file_load(READ_FILE);
    if (!ini)
    {
        int line, type;
        ini_error_info(&line, &type);
        printf("ini parse error! line %d, error %d.\r\n", line, type);
        // printf("load fail!\r\n");
        return;
    }
    printf("load success!\r\n");
    // ini_set_value(ini, "王五", "年级", "2");
    // ini_set_value(ini, "王五", "班级", "1");
    // ini_set_value(ini, "王五", "类别", "理科");
    // printf("%s\r\n", ini_key_name(ini, "王五", 2));
    // 
    #if 1
    for (int i = 0; i < ini_section_count(ini); i++)
    {
        char *s = (const char*)ini_section_name(ini, i);
        printf("section: [%s]\r\n", s);
        int count = ini_pair_count(ini, s);
        for (int j = 0; j < count; j++)
        {
            char *k = (const char*)ini_key_name(ini, s, j);
            printf("%s : %s\r\n", k, ini_get_value(ini, s, k));
        }
    }
    #endif
    ini_file_dump(ini, WRITE_FILE);
    ini_delete(ini);
    v_check_unfree();
}

static void test(void)
{
    // test_dump();
    test_load();
}
init_export_app(test);
