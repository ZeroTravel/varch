#include <stdio.h>
#include <string.h>
#include "init.h"
#include "xml.h"

#define READ_FILE "test/file/read.xml"
#define WRITE_FILE "test/file/write.xml"

static void test_read(void)
{
    xml_t root, x;
    
    root = xml_file_load(READ_FILE);
    if (!root) return;

    printf("load success!\r\n");

    x = xml_to(root, "book", 1);
    printf("x attr: %s\r\n", xml_get_attribute(x, NULL, 0));

    x = xml_to(x, "author", 0);
    printf("author: %s\r\n", xml_get_text(x));

    xml_delete(root);
}

static void test_write(void)
{
    xml_t root, x;

    root = xml_create("root");
    if (!root) return;

    x = xml_create("name");
    xml_set_text(x, "xml parser");
    xml_insert(root, 0, x);

    x = xml_create("description");
    xml_set_text(x, "This is a C language version of xml parser.");
    xml_insert(root, 1, x);

    x = xml_create("license"); 
    xml_set_text(x, "GPL3.0");
    xml_insert(root, 2, x);

    char *s = xml_dumps(root, 1, 0, NULL);
    if (s)
    {
        printf(s);
        free(s);
    }
    
    xml_file_dump(root, WRITE_FILE);

    xml_delete(root);
}

static void test(void)
{
    printf("xml test!\r\n");
    test_write();
    // test_read();

    v_check_unfree();
}
init_export_app(test);
