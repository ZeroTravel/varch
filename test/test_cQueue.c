#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "valloc.h"
#include "cQueue.h"

void test_int(void)
{
    typedef struct 
    {
        cQueue queue;
        int data[10];
    } intQueueType;
    intQueueType intQueue;

    cQueue_init(intQueue);

    for (int i = 0; i < intQueue.queue.cap; i++)
    {
        cQueue_push(intQueue, i);
    }

    printf("cQueue[5] = %d\r\n", cQueue_at(intQueue, 5));

    while (intQueue.queue.size > 0)
    {
        int data;
        cQueue_pop(intQueue, data);
        printf("cQueue_pop %d\r\n", data);
    }
}

void test_struct(void)
{
    typedef struct 
    {
        char *name;
        int age;
    } Stu;
    typedef struct 
    {
        cQueue queue;
        Stu data[10];
    } StuQueueType;
    StuQueueType StuQueue;

    Stu s = {"Zhang", 18};

    cQueue_init(StuQueue);

    for (int i = 0; i < StuQueue.queue.cap; i++)
    {
        s.age = 18 + i;
        cQueue_push(StuQueue, s);
    }

    while (StuQueue.queue.size > 0)
    {
        cQueue_pop(StuQueue, s);
        printf("cQueue_pop name: %s age %d\r\n", s.name, s.age);
    }
}

static void test(void)
{
    printf("cQueue test!\r\n");

    // test_int();
    test_struct();
    
    v_check_unfree();
}
init_export_app(test);
