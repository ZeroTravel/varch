#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "encrypt.h"
#include <stdint.h>

static void test_des(void)
{
    uint8_t key[8] = "hello";
    uint8_t data_block[8] = {1, 2, 3, 4, 5, 6, 7, 9};
    uint8_t processed_block[8];
    int i;

    des_set_key(key);

    printf("des ecb encrypt: ");
    des_crypt_ecb(data_block, processed_block, DES_ENCRYPT);
    for (i = 0; i < 8; i++)
    {
        printf("%d ", processed_block[i]);
    }
    printf("\r\n");

    printf("des ecb decrypt: ");
    des_crypt_ecb(processed_block, data_block, DES_DECRYPT);
    for (i = 0; i < 8; i++)
    {
        printf("%d ", data_block[i]);
    }
    printf("\r\n");

    ///////////////////////////////////////
    printf("des cbc encrypt: ");
    des_crypt_cbc(data_block, processed_block, 8, DES_ENCRYPT);
    for (i = 0; i < 8; i++)
    {
        printf("%d ", processed_block[i]);
    }
    printf("\r\n");

    printf("des cbc decrypt: ");
    des_crypt_cbc(processed_block, data_block, 8, DES_DECRYPT);
    for (i = 0; i < 8; i++)
    {
        printf("%d ", data_block[i]);
    }
    printf("\r\n");
}

static void test_des3(void)
{
    uint8_t key[24] = "hello world";
    uint8_t data_block[8] = {1, 2, 3, 4, 5, 6, 7, 9};
    uint8_t processed_block[8];
    int i;

    des3_set_key2(key);

    printf("des3 ecb encrypt: ");
    des3_crypt_ecb(data_block, processed_block, DES_ENCRYPT);
    for (i = 0; i < 8; i++)
    {
        printf("%d ", processed_block[i]);
    }
    printf("\r\n");

    printf("des3 ecb decrypt: ");
    des3_crypt_ecb(processed_block, data_block, DES_DECRYPT);
    for (i = 0; i < 8; i++)
    {
        printf("%d ", data_block[i]);
    }
    printf("\r\n");

    ///////////////////////////////////////
    printf("des3 cbc encrypt: ");
    des3_crypt_cbc(data_block, processed_block, 8, DES_ENCRYPT);
    for (i = 0; i < 8; i++)
    {
        printf("%d ", processed_block[i]);
    }
    printf("\r\n");

    printf("des3 cbc decrypt: ");
    des3_crypt_cbc(processed_block, data_block, 8, DES_DECRYPT);
    for (i = 0; i < 8; i++)
    {
        printf("%d ", data_block[i]);
    }
    printf("\r\n");
}

static void test(void)
{
    // test_des();
    test_des3();
}
init_export_app(test);
