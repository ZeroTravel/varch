#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"
#include "tool.h"
#include "search.h"

static void* int_array_addr(void *array, int index)
{
    return (void *)(&((int *)array)[index]);
}

static int int_cmp(void *element, void *target)
{
    if (*(int *)element > *(int *)target) return 1;
    else if (*(int *)element < *(int *)target) return -1;
    return 0;
}

static void test_linear(void)
{
    int array[] = {6, 3, 1, 0, -2, 9};
    SearchOPS ops;
    int target = 1;

    ops.addr = int_array_addr;
    ops.cmp = int_cmp;

    target = 9, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = -2, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 0, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 1, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 3, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 6, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));

    target = 5, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = -9, printf("search %d, result %d\r\n", target, search_linear(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
}

static void test_binary(void)
{
    int array[] = {0, 3, 6, 10, 62, 99};
    SearchOPS ops;
    int target = 10;

    ops.addr = int_array_addr;
    ops.cmp = int_cmp;

    target = 0, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 3, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 6, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 10, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 62, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = 99, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));

    target = 5, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
    target = -9, printf("search %d, result %d\r\n", target, search_binary(array, 0, sizeof(array) / sizeof(array[0]) - 1, &target, &ops));
}

static void test(void)
{
    printf("search test!\r\n");
    
    // test_linear();
    test_binary();
}
init_export_app(test);
