#include <stdio.h>
#include <string.h>
#include "init.h"
#include "tool.h"

static void test_GetStringHex(void)
{
    char out[12];
    for (int i = 0; i < 3; i++)
    {
        int len = GetStringHex("12 34 56 78 89 ", -1, out, 12);
        if (len >= 0)
        {
            printf("len %d\r\n", len);
            for (int i = 0; i < len; i++)
            {
                printf("%02X ", (char)(out[i]) & 0xFF);
            }
            printf("\r\n");
        }
        else
        {
            printf("error %d\r\n", len);
        }
    }
    
    char a[4] = {0x10, 0xFF, 0x45, 0x1F};
    char buffer[1024];
    ToStringHex(a, 4, buffer, 1024);
    printf("%s\r\n", buffer);
}

static void test_ASSERT(void)
{
    ASSERT(0 == 1);
}

static void test_print(void)
{
    char c = 'c';
    int i = 123;
    float f = 3.14;
    char *s = "string";

    printChar(c);
    printInt(i);
    printFloat(f);
    printString(s);
}

static void test(void)
{
    // test_GetStringHex();
    // test_ASSERT();
    test_print();
}
init_export_app(test);
