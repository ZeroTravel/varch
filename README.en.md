# varch  

![logo](/image/logo.png) 

## Introduction

[中文版](README.md) 

varch (we architecture, meaning our framework library) is a commonly used code module library for embedded C language, including algorithm libraries, data structure (container) libraries, parser libraries, independent C language std libraries, tool libraries, and more.
It has the characteristics of **simplicity, universality, and efficiency**, with the aim of **learning** and **using it immediately** in development, improving development efficiency and code reliability and stability.

## Content  

| module       | version  | usage                        | path                                  | describe                              |
|:-------------|:---------|:-----------------------------|:--------------------------------------|:--------------------------------------|
| overall      | 00.02.00 | [link](README.md)            | ./                                    | Overall
| init         | 01.00.00 | [link](/doc/init.md)         | ./source/00_application               | Initialize export module
| console      | 01.00.00 | [link](/doc/console.md)      | ./source/00_application/console       | Console command input, combined with the 'command' module, parsing commands entered in the console
| arg          | 01.00.00 | [link](/doc/arg.md)          | ./source/01_general                   | Indefinite parameters, obtain the number of indefinite parameters and specified parameters
| calculate    | 01.00.00 | [link](/doc/calculate.md)    | ./source/01_general                   | Calculation module, input calculation expression to obtain calculation result
| command      | 01.00.00 | [link](/doc/command.md)      | ./source/01_general                   | Command parsing module, input string commands (similar to shell commands), execute corresponding command functions
| cPatten      | 01.00.00 | [link](/doc/cPatten.md)      | ./source/01_general                   | Artistic character patterns
| cQueue       | 01.00.00 | [link](/doc/cQueue.md)       | ./source/01_general                   | Universal queue controller
| dList        | 01.00.00 | [link](/doc/dList.md)        | ./source/01_general                   | Universal double-link list controller
| fsm          | 01.00.00 | [link](/doc/fsm.md)          | ./source/01_general                   | Universal finite state machine module
| kern         | 01.00.00 | [link](/doc/kern.md)         | ./source/01_general                   | The kernel module for scheduling periodic tasks is mostly used in varch testing
| oscp         | 01.00.00 | [link](/doc/oscp.md)         | ./source/01_general                   | Analog oscilloscope module, can easily monitor the waveform of variable changes
| sList        | 01.00.00 | [link](/doc/sList.md)        | ./source/01_general                   | Universal single-link list controller
| tool         | 01.00.00 | [link](/doc/tool.md)         | ./source/01_general                   | General tools code
| valloc       | 01.00.00 | [link](/doc/valloc.md)       | ./source/01_general                   | Dynamic memory usage testing tool
| vlog         | 01.01.00 | [link](/doc/vlog.md)         | ./source/01_general                   | Log output module
| vctype       | 01.00.00 | [link](/doc/vctype.md)       | ./source/02_vstd                      | Similar to the C standard library ctype
| vmath        | 01.00.00 | [link](/doc/vmath.md)        | ./source/02_vstd                      | Similar to the C standard library math
| vmem         | 01.00.00 | [link](/doc/vmem.md)         | ./source/02_vstd                      | Simple implementation of memory pool
| vstddef      | 01.00.00 | [link](/doc/vstddef.md)      | ./source/02_vstd                      | Similar to the C standard library stddef
| vstdint      | 01.00.00 | [link](/doc/vstdint.md)      | ./source/02_vstd                      | Similar to the C standard library stdint
| vstdlib      | 01.00.00 | [link](/doc/vstdlib.md)      | ./source/02_vstd                      | Similar to the C standard library stdlib
| vstring      | 01.00.00 | [link](/doc/vstring.md)      | ./source/02_vstd                      | Similar to the C standard library string
| deque        | 01.00.00 | [link](/doc/deque.md)        | ./source/03_container                 | Universal double-end queue container
| dict         | 01.00.00 | [link](/doc/dict.md)         | ./source/03_container                 | Universal dictionarie container, implementation based on hash table
| heap         | 01.00.00 | [link](/doc/heap.md)         | ./source/03_container                 | Universal heap container
| list         | 01.00.00 | [link](/doc/list.md)         | ./source/03_container                 | Universal list container, single-link and internal iteration
| map          | 01.00.00 | [link](/doc/map.md)          | ./source/03_container                 | Universal map container, implementation based on RB-tree
| queue        | 01.00.00 | [link](/doc/queue.md)        | ./source/03_container                 | Universal queue container
| set          | 01.00.00 | [link](/doc/set.md)          | ./source/03_container                 | Universal set container, implementation based on RB-tree
| stack        | 01.00.00 | [link](/doc/stack.md)        | ./source/03_container                 | Universal stack container
| str          | 01.00.00 | [link](/doc/str.md)          | ./source/03_container                 | String class
| tree         | 01.00.00 | [link](/doc/tree.md)         | ./source/03_container                 | Universal tree container
| vector       | 01.00.00 | [link](/doc/vector.md)       | ./source/03_container                 | Universal vector(array) container
| graph        | 01.00.00 | [link](/doc/graph.md)        | ./source/03_container                 | Universal graph container
| check        | 01.00.00 | [link](/doc/check.md)        | ./source/04_algorithm                 | Verification algorithm, sum check, parity check, XOR check, LRC check
| crc          | 01.00.00 | [link](/doc/check.md)        | ./source/04_algorithm                 | Universal and standard CRC algorithms
| encrypt      | 01.00.00 | [link](/doc/encrypt.md)      | ./source/04_algorithm                 | Encryption and decryption algorithms
| filter       | 01.00.00 | [link](/doc/filter.md)       | ./source/04_algorithm                 | Filter algorithms, median, kalman, average
| hash         | 01.00.00 | [link](/doc/hash.md)         | ./source/04_algorithm                 | Hash algorithms, bkdr, ap, djb, js, rs, sdbm, pjw, elf, dek, bp, fnv, jdk6
| pid          | 01.00.00 | [link](/doc/pid.md)          | ./source/04_algorithm                 | PID control algorithm calculator
| search       | 01.00.00 | [link](/doc/search.md)       | ./source/04_algorithm                 | Universal search algorithms, linear, binary
| sort         | 01.00.00 | [link](/doc/sort.md)         | ./source/04_algorithm                 | Universal sorting algorithms (various data structures), bubble, select, insert, hill, quick, heap
| csv          | 01.00.00 | [link](/doc/csv.md)          | ./source/05_parser                    | CSV file parsing generator
| ini          | 01.00.00 | [link](/doc/ini.md)          | ./source/05_parser                    | INI configuration file parsing generator
| json         | 01.00.00 | [link](/doc/json.md)         | ./source/05_parser                    | JSON file parsing generator
| txls         | 01.00.00 | [link](/doc/txls.md)         | ./source/05_parser                    | TXLS file parsing generator
| xml          | 01.00.00 | [link](/doc/xml.md)          | ./source/05_parser                    | XML file parsing generator

## Usage

The code is compiled and tested in a Linux environment. It can be compiled by configuring the files to be compiled in the `makefile` directory under the `build` directory, or by directly running the `run. sh` file to compile and run. The varch module should be kept as independent as possible, and in order to reduce dependence on other modules, most files can be directly extracted and used separately. If there are dependencies on other modules during compilation to solve dependency problems, but only data type dependencies, you can refer to defining the required types.

## License  

                    GNU GENERAL PUBLIC LICENSE
                       Version 2, June 1991

## Contach  

Lamdonn@163.com  

