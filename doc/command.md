## 介绍  

命令提示符是在操作系统中，提示进行命令输入的一种工作提示符，是一个便利人机交互接口。  
该命令解析模块，暂不涉及接收从控制台传入的命令，而是处理字符串命令。  

## 接口  

### 命令导出
```c
int command_export(const char *name, command_handle_t handle);
```
在介绍命令解析函数之前，先看一下命令导出函数。  
这个函数就是添加支持的函数，通过名字和相应的回调函数对应起来。`name`函数识别名（一般就是用函数名当作命令解析的识别名），`handle`就是相应的回调处理函数。导出成功了就返回1，失败返回0。  
```c
typedef int (*command_handle_t)(int argc, char *argv[]);
```
`command_handle_t`函数类型的定义就是和我们常用的`int main(int argc, char *argv[])`是一致的，参数通过`argc`和`argv`传入。 

### 命令执行  
```c
int command(const char *line);
```
命令执行函数也很简单，也就是传入命令行就OK，返回相应命令识别的函数的返回值，-1是解析失败的时候的返回值，所以在定义处理函数时候，**返回值不能使用-1**。  
命令的输入格式：  
```
argv[0] argv[1] argv[2] ...
```
其中`argv[0]`就是命令识别符，后面可以跟若干个参数，个数通过`argc`传给函数。  
命令参数通过空格分开，而当想在参数内传入空格时候，就需要用到转义字符反斜杠`\`进行转义，"\ "表示空格，除此，还支持的**转义字符**包含：双引号`"`和反斜杠`\`。   
为了简化转义字符的使用，可以用**一对双引号**来括住不需要转义解析的命令。  
使用例子：  
```c
int func1(int argc, char *argv[])
{
	printf("I am func1!\r\n");
	printf("argc = %d\r\n", argc);
	for (int i = 0; i < argc; i++)
	{
		printf("argv[%d] = %s\r\n", i, argv[i]);
	}
	return 1;
}

int func2(int argc, char *argv[])
{
	printf("I am func2!\r\n");
	return 1;
}

int func3(int argc, char *argv[])
{
	printf("I am func3!\r\n");
	return 1;
}

static void test(void)
{
    /* 导出命令 */
	command_export("func1", func1);
	command_export("func2", func2);
	command_export("func3", func3);

	command("cmd -l"); // 模块内置的命令，用于查看当前支持的命令
	printf("--------------------------\r\n");
	command("func2");
	printf("--------------------------\r\n");
	command("func3");
	printf("--------------------------\r\n");
	command("func1 1 2 3"); // 不转义，每个空格分隔的都将成为单独的参数
	printf("--------------------------\r\n");
	command("func1 1\\ 2 3"); // 1后面的空格进行转义，1和2之前将会和空格连在一起
	printf("--------------------------\r\n");
	command("func1 \"1 2 3\""); // 双引号之间 "1 2 3" 会被解析为一个参数
}
```
结果：  
```
cmd
func1
func2
func3
count = 4
--------------------------
I am func2!
--------------------------
I am func3!
--------------------------
I am func1!
argc = 4
argv[0] = func1
argv[1] = 1
argv[2] = 2
argv[3] = 3
--------------------------
I am func1!
argc = 3
argv[0] = func1
argv[1] = 1 2
argv[2] = 3
--------------------------
I am func1!
argc = 2
argv[0] = func1
argv[1] = 1 2 3
```
在其中`cmd`为内置命令，统计当前支持的命令，返回支持的命令个数。  

### 命令清空  
```c
void command_clear(void);
```
清空所有导出的命令。  
