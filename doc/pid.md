## 介绍  

PID算法，即比例-积分-微分算法，是一种广泛应用于工业控制系统中的反馈控制算法。它通过比较期望值和实际值之间的误差，然后根据误差的比例（P）、积分（I）和微分（D）来调整控制器的输出，从而使系统的输出更加接近期望值。

PID算法的基本形式如下：

u(t) = Kp * e(t) + Ki * ∫e(t)dt + Kd * de(t)/dt

其中，u(t)是控制器的输出，e(t)是期望值和实际值之间的误差，Kp、Ki和Kd分别是比例、积分和微分系数，它们需要根据具体系统进行调整以达到最佳控制效果。

PID算法的优点在于其简单易用，适用范围广泛，且可以通过调整参数来适应不同的系统特性。但是，对于一些复杂的非线性系统，PID算法可能无法达到理想的控制效果。   

## 接口  

```c
void pid_init(PIDC *pid);
```
在执行PID算法计算之前，需初始化pid算法控制结构体。

```c
double pid_compute(PIDC *pid);
```
在执行PID算法计算之前，需初始化pid算法控制结构体。

## 测试  

```c
static void test_sim(void)
{
    PIDC pid;

    pid_init(&pid);

    pid.kp = 2;
    pid.ki = 0.1;
    pid.kd = 0.05;

    pid.point = 100.0;

    /* Simulate the process, assuming that the value of each cycle process increases by 5 */
    for (int i = 0; i < 20; i++) 
    {
        pid.process += 5;
        pid_compute(&pid);

        printf("Process Value: %.2f, PID Output: %.2f\n", pid.process, pid.output);
    }
}
```

结果  

```
Process Value: 5.00, PID Output: 204.25
Process Value: 10.00, PID Output: 198.25
Process Value: 15.00, PID Output: 196.75
Process Value: 20.00, PID Output: 194.75
Process Value: 25.00, PID Output: 192.25
Process Value: 30.00, PID Output: 189.25
Process Value: 35.00, PID Output: 185.75
Process Value: 40.00, PID Output: 181.75
Process Value: 45.00, PID Output: 177.25
Process Value: 50.00, PID Output: 172.25
Process Value: 55.00, PID Output: 166.75
Process Value: 60.00, PID Output: 160.75
Process Value: 65.00, PID Output: 154.25
Process Value: 70.00, PID Output: 147.25
Process Value: 75.00, PID Output: 139.75
Process Value: 80.00, PID Output: 131.75
Process Value: 85.00, PID Output: 123.25
Process Value: 90.00, PID Output: 114.25
Process Value: 95.00, PID Output: 104.75
Process Value: 100.00, PID Output: 94.75
```