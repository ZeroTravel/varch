## 介绍  

vlog是一个简单的日志输出模块，采用的是通道过滤模式，也就是提供了多个通道的日志输出，只有该通道使能了才会有日志输出。     

## 接口  

### vlog日志输出

```c
int vlog(vlogChnType channel, const char *format, ...);
```

channel提供了8个通道
```
#define VLOG_CHANNEL_0
#define VLOG_CHANNEL_1
#define VLOG_CHANNEL_2
#define VLOG_CHANNEL_3
#define VLOG_CHANNEL_4
#define VLOG_CHANNEL_5
#define VLOG_CHANNEL_6
#define VLOG_CHANNEL_7
```

可以进行多通道的输出，通过 `|` 连接则进行多通道的输出。    
默认是只打开了 `VLOG_CHANNEL_0`。

```c
static void test_vlog(void)
{
    vlog(VLOG_CHANNEL_0,   "[VLOG_CHANNEL_0] vlog!\r\n");
    vlog(VLOG_CHANNEL_1,   "[VLOG_CHANNEL_1] vlog!\r\n");
    vlog(VLOG_CHANNEL_2,   "[VLOG_CHANNEL_2] vlog!\r\n");
}
```
结果： 
```
[VLOG_CHANNEL_0] vlog!
```

### vlog设置和获取通道过滤器

```c
void vlog_set_filter(vlogChnType mask);
vlogChnType vlog_get_filter(void);
```

其分别可以设置和获取开放的通道过滤器。
为了更简便，提供了如下宏定义方法可以打开和关闭指定通道。  

```c
#define VLOG_ENABALE(c) 
#define VLOG_DISABALE(c)
```

```c
static void test_channel(void)
{
    VLOG_DISABALE(VLOG_CHANNEL_0);
    VLOG_ENABALE(VLOG_CHANNEL_1);

    vlog(VLOG_CHANNEL_0,   "[VLOG_CHANNEL_0] vlog!\r\n");
    vlog(VLOG_CHANNEL_1,   "[VLOG_CHANNEL_1] vlog!\r\n");
    vlog(VLOG_CHANNEL_2,   "[VLOG_CHANNEL_2] vlog!\r\n");
}
```
结果： 
```
[VLOG_CHANNEL_1] vlog!
```

### vlog控制台输出

```c
int vlog_set_console(vlogChnType channel, int console);
```

此方法是设置指定通道是否开启控制台输出，默认各个通道都已经打开控制台输出。

### vlog离线保存 

```c
int vlog_set_offline(vlogChnType channel, const char *filename);
```

此方法可以开始离线保存和停止离线保存（传入空文件名）。  

```c
static void test_offline(void)
{
    vlog_set_offline(VLOG_CHANNEL_0, "./test/file/log.txt");

    for (int i = 0; i < 10; i++)
    {
        vlog(VLOG_CHANNEL_0, "vlog %d!\r\n", i);
    }

    vlog_set_offline(VLOG_CHANNEL_0, NULL);
}
```
结果： 
[link](/test/file/log.txt)

### vlog函数回调

```c
int vlog_set_func(vlogChnType channel, vlogFuncType func);
```

此方法设置vlog回调函数，提供了更大的灵活度。也就是函数vlog内容会输出到回调函数。  

```c
typedef void (*vlogFuncType)(char *buf, int len);
```

### vlog获取通道状态 

```c
vlogChnType vlog_get_console(void);
vlogChnType vlog_get_offline(void);
vlogChnType vlog_get_func(void);
```

其分别可以获取3种输出方向的各个通道状态掩码。 
