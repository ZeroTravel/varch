## 介绍  

队列是一种先进先出(First In First Out,FIFO)的特殊数据结构，一般情况下它只有一个出口一个入口，从队尾进入从队头出，入队push，出队pop。 

cQueue为一个队列控制体，自身本不是容器，和varch的queue有区别，但是使用上却很类似。     

## 接口  

### 定义队列类型

因为cQueue本身并不是数据容器，所以需要对其存储的数据类型定义一个新的数据结构，如下例子

```c
typedef struct 
{
    cQueue queue;               // 第一个成员，默认为这个，无需改动
    int data[10];               // 第二个成员，定义实际存储的数据数组，数组类型为任意类型，数组名都为 data ， 容量根据实际所需
} intQueueType;                 // 定义一个新的数据结构体类型
intQueueType intQueue;          // 定义一个新的结构体变量
```

### 初始化队列

```c
#define cQueue_init(qObject)
```

宏定义方法，能适配各种数据类型
 
```c
void test_int(void)
{
    typedef struct 
    {
        cQueue queue;
        int data[10];
    } intQueueType;
    intQueueType intQueue;

    cQueue_init(intQueue);
}
```

### 队列空队和满队  

```c
#define cQueue_empty(qObject)
#define cQueue_full(qObject)
```

这两个方法实际就是queue的`size`的大小关系，等于0为空，等于容量则满。  

### 队列数据入队和出队    

```c
#define cQueue_push(qObject, d)
#define cQueue_pop(qObject, d)
```

这两个方法分别将数据d入队，和从队列出队数据到d  
 
```c
void test_int(void)
{
    typedef struct 
    {
        cQueue queue;
        int data[10];
    } intQueueType;
    intQueueType intQueue;

    cQueue_init(intQueue);

    for (int i = 0; i < intQueue.queue.cap; i++)
    {
        cQueue_push(intQueue, i);
    }

    while (intQueue.queue.size > 0)
    {
        int data;
        cQueue_pop(intQueue, data);
        printf("cQueue_pop %d\r\n", data);
    }
}
```
结果： 
```
cQueue_pop 0
cQueue_pop 1
cQueue_pop 2
cQueue_pop 3
cQueue_pop 4
cQueue_pop 5
cQueue_pop 6
cQueue_pop 7
cQueue_pop 8
cQueue_pop 9
```

### 队列数据访问  

```c
#define cQueue_at(qObject, i)
```

随机访问队列中的数据。  

```c
void test_int(void)
{
    typedef struct 
    {
        cQueue queue;
        int data[10];
    } intQueueType;
    intQueueType intQueue;

    cQueue_init(intQueue);

    for (int i = 0; i < intQueue.queue.cap; i++)
    {
        cQueue_push(intQueue, i);
    }

    printf("cQueue[5] = %d\r\n", cQueue_at(intQueue, 5));
}
```
结果： 
```
cQueue[5] = 5
```

## 特殊数据结构体例子  

```c
void test_struct(void)
{
    typedef struct 
    {
        char *name;
        int age;
    } Stu;
    typedef struct 
    {
        cQueue queue;
        Stu data[10];
    } StuQueueType;
    StuQueueType StuQueue;

    Stu s = {"Zhang", 18};

    cQueue_init(StuQueue);

    for (int i = 0; i < StuQueue.queue.cap; i++)
    {
        s.age = 18 + i;
        cQueue_push(StuQueue, s);
    }

    while (StuQueue.queue.size > 0)
    {
        cQueue_pop(StuQueue, s);
        printf("cQueue_pop name: %s age %d\r\n", s.name, s.age);
    }
}
```
结果： 
```
cQueue_pop name: Zhang age 18
cQueue_pop name: Zhang age 19
cQueue_pop name: Zhang age 20
cQueue_pop name: Zhang age 21
cQueue_pop name: Zhang age 22
cQueue_pop name: Zhang age 23
cQueue_pop name: Zhang age 24
cQueue_pop name: Zhang age 25
cQueue_pop name: Zhang age 26
cQueue_pop name: Zhang age 27
```
