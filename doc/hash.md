## 介绍  

Hash 算法能将将任意长度的数据离散映射到较短指定长度的数据的算法，并且一般不可逆。   

## 接口  

```c
uint32_t hash_bkdr(void *data, uint32_t size);
uint32_t hash_ap(void *data, uint32_t size);
uint32_t hash_djb(void *data, uint32_t size);
uint32_t hash_js(void *data, uint32_t size);
uint32_t hash_rs(void *data, uint32_t size);
uint32_t hash_sdbm(void *data, uint32_t size);
uint32_t hash_pjw(void *data, uint32_t size);
uint32_t hash_elf(void *data, uint32_t size);
uint32_t hash_dek(void *data, uint32_t size);
uint32_t hash_bp(void *data, uint32_t size);
uint32_t hash_fnv(void *data, uint32_t size);
uint32_t hash_jdk6(void *data, uint32_t size);
```
这几种校验算法使用方法一致，都是传入数据地址和数据大小，返回计算出来的32bits哈希值。 

## 测试  

```c
static void test(void)
{
	printf("hash_bkdr 0x%X\r\n", hash_bkdr("Hello", 5));
	printf("hash_ap 0x%X\r\n", hash_ap("Hello", 5));
	printf("hash_djb 0x%X\r\n", hash_djb("Hello", 5));
	printf("hash_js 0x%X\r\n", hash_js("Hello", 5));
	printf("hash_rs 0x%X\r\n", hash_rs("Hello", 5));
	printf("hash_sdbm 0x%X\r\n", hash_sdbm("Hello", 5));
	printf("hash_pjw 0x%X\r\n", hash_pjw("Hello", 5));
	printf("hash_elf 0x%X\r\n", hash_elf("Hello", 5));
	printf("hash_dek 0x%X\r\n", hash_dek("Hello", 5));
	printf("hash_bp 0x%X\r\n", hash_bp("Hello", 5));
	printf("hash_fnv 0x%X\r\n", hash_fnv("Hello", 5));
	printf("hash_jdk6 0x%X\r\n", hash_jdk6("Hello", 5));
}
```

结果  

```
hash_bkdr 0x7D80646E
hash_ap 0x646C7322
hash_djb 0xD4F2079
hash_js 0x6060CD85
hash_rs 0x15794872
hash_sdbm 0x2B45B912
hash_pjw 0x4EC32F
hash_elf 0x4EC32F
hash_dek 0xEB33DEF
hash_bp 0xCBB366F
hash_fnv 0x2EA7AFEE
hash_jdk6 0x42628B2
```