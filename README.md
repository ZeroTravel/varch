# varch  

![logo](/image/logo.png) 

## 介绍

[English version](README.en.md) 

varch（we-architecture，意为我们的框架库）是嵌入式C语言常用代码模块库，包含了嵌入式中常用的算法库, 数据结构（容器）库, 解析器库, 独立C语言std库, 工具库等等。    
具有**简单, 通用, 高效**的特点，目的为了**学习**以及在开发中**拿来就用**，提高开发效率以及代码可靠稳定性。  

## 内容  

| module       | version  | usage                        | path                                  | describe                              |
|:-------------|:---------|:-----------------------------|:--------------------------------------|:--------------------------------------|
| overall      | 00.02.00 | [link](README.md)            | ./                                    | 整体
| init         | 01.00.00 | [link](/doc/init.md)         | ./source/00_application               | 初始化导出模块
| console      | 01.00.00 | [link](/doc/console.md)      | ./source/00_application/console       | 控制台命令输入，结合 `command` 模块，解析在控制台中输入的命令
| arg          | 01.00.00 | [link](/doc/arg.md)          | ./source/01_general                   | 不定参数，获取不定参数和指定参数的个数
| calculate    | 01.00.00 | [link](/doc/calculate.md)    | ./source/01_general                   | 计算模块，输入计算表达式，得到计算结果
| command      | 01.00.00 | [link](/doc/command.md)      | ./source/01_general                   | 命令解析模块，输入字符串命令(类似于shell命令)，执行相应的命令功能
| cPatten      | 01.00.00 | [link](/doc/cPatten.md)      | ./source/01_general                   | 艺术图案字符
| cQueue       | 01.00.00 | [link](/doc/cQueue.md)       | ./source/01_general                   | 通用队列控制器
| dList        | 01.00.01 | [link](/doc/dList.md)        | ./source/01_general                   | 通用双链表控制器
| fsm          | 01.00.00 | [link](/doc/fsm.md)          | ./source/01_general                   | 通用有限状态机模块
| kern         | 01.00.00 | [link](/doc/kern.md)         | ./source/01_general                   | 用于调度周期性任务的内核模块主要用于varch测试
| oscp         | 01.00.00 | [link](/doc/oscp.md)         | ./source/01_general                   | 模拟示波器模块，可以方便地监测波形的变量变化
| sList        | 01.00.01 | [link](/doc/sList.md)        | ./source/01_general                   | 通用单链表控制器
| tool         | 01.00.00 | [link](/doc/tool.md)         | ./source/01_general                   | 通用工具代码
| valloc       | 01.00.00 | [link](/doc/valloc.md)       | ./source/01_general                   | 动态内存使用测试工具
| vlog         | 01.01.00 | [link](/doc/vlog.md)         | ./source/01_general                   | 日志输出模块
| vctype       | 01.00.00 | [link](/doc/vctype.md)       | ./source/02_vstd                      | 类似于C标准库ctype
| vmath        | 01.00.00 | [link](/doc/vmath.md)        | ./source/02_vstd                      | 类似于C标准库math
| vmem         | 01.00.00 | [link](/doc/vmem.md)         | ./source/02_vstd                      | 内存池的简单实现
| vstddef      | 01.00.00 | [link](/doc/vstddef.md)      | ./source/02_vstd                      | 类似于C标准库stddef
| vstdint      | 01.00.00 | [link](/doc/vstdint.md)      | ./source/02_vstd                      | 类似于C标准库stdint
| vstdlib      | 01.00.00 | [link](/doc/vstdlib.md)      | ./source/02_vstd                      | 类似于C标准库stdlib
| vstring      | 01.00.00 | [link](/doc/vstring.md)      | ./source/02_vstd                      | 类似于C标准库string
| deque        | 01.00.00 | [link](/doc/deque.md)        | ./source/03_container                 | 通用双端队列容器
| dict         | 01.00.00 | [link](/doc/dict.md)         | ./source/03_container                 | 通用字典容器，基于哈希表实现
| heap         | 01.00.00 | [link](/doc/heap.md)         | ./source/03_container                 | 通用堆容器
| list         | 01.00.00 | [link](/doc/list.md)         | ./source/03_container                 | 通用列表容器，单链接和支持内部迭代器
| map          | 01.00.00 | [link](/doc/map.md)          | ./source/03_container                 | 通用映射容器，基于RB-tree实现
| queue        | 01.00.00 | [link](/doc/queue.md)        | ./source/03_container                 | 通用队列容器
| set          | 01.00.00 | [link](/doc/set.md)          | ./source/03_container                 | 通用集合容器，基于RB-tree实现
| stack        | 01.00.00 | [link](/doc/stack.md)        | ./source/03_container                 | 通用栈式容器
| str          | 01.00.00 | [link](/doc/str.md)          | ./source/03_container                 | 字符串类
| tree         | 01.00.00 | [link](/doc/tree.md)         | ./source/03_container                 | 通用树容器
| vector       | 01.00.00 | [link](/doc/vector.md)       | ./source/03_container                 | 通用向量(数组)容器
| graph        | 01.00.00 | [link](/doc/graph.md)        | ./source/03_container                 | 通用图容器
| check        | 01.00.00 | [link](/doc/check.md)        | ./source/04_algorithm                 | 校验算法，求和校验，奇偶校验，异或校验，LRC校验
| crc          | 01.00.00 | [link](/doc/check.md)        | ./source/04_algorithm                 | 通用标准CRC算法
| encrypt      | 01.00.00 | [link](/doc/encrypt.md)      | ./source/04_algorithm                 | 加密解密算法
| filter       | 01.00.00 | [link](/doc/filter.md)       | ./source/04_algorithm                 | 滤波算法，中值，卡尔曼，平均值
| hash         | 01.00.00 | [link](/doc/hash.md)         | ./source/04_algorithm                 | 哈希算法，bkdr、ap、djb、js、rs、sdbm、pjw、elf、dek、bp、fnv、jdk6
| pid          | 01.00.00 | [link](/doc/pid.md)          | ./source/04_algorithm                 | PID控制算法计算器
| search       | 01.00.00 | [link](/doc/search.md)       | ./source/04_algorithm                 | 通用搜索算法，线性，二进制
| sort         | 01.00.00 | [link](/doc/sort.md)         | ./source/04_algorithm                 | 通用排序算法(各种数据结构)，冒泡排序、选择排序、插入排序、希尔排序、快速排序、堆排序
| csv          | 01.00.00 | [link](/doc/csv.md)          | ./source/05_parser                    | CSV文件解析生成器
| ini          | 01.00.00 | [link](/doc/ini.md)          | ./source/05_parser                    | INI配置文件解析生成器
| json         | 01.00.00 | [link](/doc/json.md)         | ./source/05_parser                    | JSON文件解析生成器
| txls         | 01.00.00 | [link](/doc/txls.md)         | ./source/05_parser                    | TXLS文件解析生成器
| xml          | 01.00.00 | [link](/doc/xml.md)          | ./source/05_parser                    | XML文件解析生成器

## 使用说明

代码在linux环境下编写编译测试，在`built`目录下的`makefile`配置需要编译的文件进行编译即可，也可以直接运行`run.sh`文件编译加运行。varch模块尽可能的保持独立，为了减少对其他模块的依赖，大部分的文件是可以直接单独拎出来就可以直接使用。如果编译存在对其他模块的依赖解决依赖问题，只是数据类型依赖的问题，完全可以参考定义所需类型即可。  

## 开源协议  

                    GNU GENERAL PUBLIC LICENSE
                       Version 2, June 1991

## 联系方式  

Lamdonn@163.com  

